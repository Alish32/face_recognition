import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    sessionId: '',

    targetExt: '',
    sourceExt: '',

    targetImg: '',
    sourceImg: '',
  },
  mutations: {
    setSessionId(state, sessionId) {
      state.sessionId = sessionId
    },
    setTargetExt(state, targetExt) {
      state.targetExt = targetExt
    },
    setSourceExt(state, sourceExt) {
      state.sourceExt = sourceExt
    },
    setTargetImg(state, targetImg) {
      state.targetImg = targetImg
    },
    setSourceImg(state, sourceImg) {
      state.sourceImg = sourceImg
    },
  },
  actions: {
    updateSessionId({ commit }, sessionId) {
      commit('setSessionId', sessionId)
    },
    updateTargetExt({ commit }, targetExt) {
      commit('setTargetExt', targetExt)
    },
    updateSourceExt({ commit }, sourceExt) {
      commit('setSourceExt', sourceExt)
    },
    updateTargetImg({ commit }, targetImg) {
      commit('setTargetImg', targetImg)
    },
    updateSourceImg({ commit }, sourceImg) {
      commit('setSourceImg', sourceImg)
    },
  },
  modules: {
  }
})
