import axios from 'axios';
const API_URL = 'https://api.datalearning.az'

export class APIService {
    getSessionId(data) {
        const url = `${API_URL}/getSessionId`;
        return axios.post(url, data);
    }
    // getBranches(token) {
    //     const url = `${API_URL}/branches`;
    //     return axios.get(url, token);
    // }
    postFile(data) {
        const url = `${API_URL}/faceidentify/postFile`;
        return axios.post(url, data);
    }
    identify(data) {
        const url = `${API_URL}/faceidentify/identify`;
        return axios.post(url, data);
    }
    getFile(id) {
        const url = `${API_URL}/faceidentify/getFile/${id}R.jpg`;
        return axios.get(url);
    }
    verfiedcount() {
        const url = `${API_URL}/faceidentify/verfiedcount`;
        return axios.post(url, {});
    }
}